'''

https://github.com/deepmind/open_spiel/blob/master/open_spiel/spiel.h


https://github.com/deepmind/open_spiel/blob/master/open_spiel/games/connect_four_test.cc

TODO mcts
https://github.com/deepmind/open_spiel/blob/master/open_spiel/python/examples/mcts.py


https://openspiel.readthedocs.io/en/latest/concepts.html

# Play a game against a random or MCTS bot:
python3 open_spiel/python/examples/mcts.py --game=tic_tac_toe --player1=human --player2=random
python3 open_spiel/python/examples/mcts.py --game=tic_tac_toe --player1=human --player2=mcts
s
'''

import random
import pyspiel

# Global constant
NUM_PLAYERS = 3



class N_Connect4_State:
    '''
        Warning: there are two distinct types of player ids!
        One at the level of Source of N_Connect4 game, and one at the level
        of Connect4 game.
        The currrent id for the Connect4 game is still stored in the attribute
        self._basic_connect4_state 
        The currrent id for the N_Connect4 game is stored in the attribute
        self._current_player of the instance of the N_Connect4_State.
        
        
    '''
    
    def __str__(self):
        return self._basic_connect4_state.__str__()+'\nTurn : player {}'.format(self.current_player())

        
    def set_current_player(self, player_id):
        '''
        Set current player id for the N_Connect4 state.
        
        Parameters
        ----------
        player_id : int in the range [0, NUM_PLAYERS)
            id of player.

        Returns
        -------
        None.

        '''
        self._current_player = player_id


    def __init__(self, basic_connect4_state):
        self._basic_connect4_state = basic_connect4_state
        self.set_current_player(0)
    
        
    def action_to_string(self, arg0, arg1=None):
        """Action -> string. Args either (player, action) or (action)."""
        # player = self.current_player() if arg1 is None else arg0
        action = arg0 if arg1 is None else arg1            
        return self._basic_connect4_state.action_to_string(action)
    
    
    def clone(self):
        my_clone = N_Connect4_State(self._basic_connect4_state.clone())
        my_clone.set_current_player(self.current_player())
        return my_clone

        
    def current_player(self):
        return self._current_player
    
    
    def legal_actions(self, player_id=None):
        '''
        Return the list of legal actions.
        For this game, the list is independent of the player.
        This is the list of the column indices that can be played
        
        Parameters
        ----------
        player_id : TYPE, optional
            ignored. The default is None.
            The parameter is there for compatibilty with generic functions
        Returns
        -------
        TYPE
            list of integers (column indices of the player can play).

        '''
        return self._basic_connect4_state.legal_actions()
    
    
    def legal_actions_mask(self, player_id=None):
        return self._basic_connect4_state.legal_actions_mask()
        

    def observation_tensor(self):
        '''
        
        Return the observation tensor with respect to the current player
        in the sense that the first 6*7 entries of the returned vector
        correspond to the token of the current player.

        Returns
        -------
        v observation tensor (1D tensor).

        '''
        bv = self._basic_connect4_state.observation_tensor()
        vx = bv[:6*7]  # x tokens (first player in basic game)
        vo = bv[6*7:2*6*7] # o tokens (2nd player in basic game)
        ve = bv[-6*  7:] # empty cells
        if self.current_player() % 2 == 0:
            return vx+vo+ve
        else:
            return vo+vx+ve
        
    
    def apply_action(self, action):
        self._basic_connect4_state.apply_action(action)
        # circular order for the players
        # compute next player index
        self._current_player = (self._current_player + 1) % NUM_PLAYERS
        
        
    def is_chance_node(self):
        return False
    
    
    def is_initial_state(self):
        return self._basic_connect4_state.is_initial_state()
    
    
    def is_terminal(self):
        return self._basic_connect4_state.is_terminal()
    
    
    def is_simultaneous_node(self):
        return False
    
    
    def is_player_node(self):
        return True


    def num_players(self):
        return NUM_PLAYERS
        
    def returns(self):
        '''
        Return list of returns
        if state is not a terminal
            0 for everyone 
        if state is terminal
           1 for the winner
           -1/(NUM_PLAYERS-1)  for the other players

        Returns L
        -------
        TYPE
            a list of floats.

        where L[i]  is the return for the ith player
        '''
        if not self.is_terminal():
            return [0]*NUM_PLAYERS
        # state is terminal
        last_player = (self.current_player() -1) % NUM_PLAYERS
        L = [1 if i == last_player else -1/(NUM_PLAYERS-1) 
                                                for i in range(NUM_PLAYERS)]
        return L

        
class N_Connect4_Game:
    '''
    
    Class for a connect4 circular game with NUM_PLAYERS
    
    '''
    
    def __init__(self):
        self.connect4_board = pyspiel.load_game("connect_four")
        
    def max_game_length(self):
        return self.connect4_board.max_game_length()
    
    def new_initial_state(self):
        return N_Connect4_State(self.connect4_board.new_initial_state())
    
    def num_players(self):
        return NUM_PLAYERS
    
    def get_type(self):
        return self.connect4_board.get_type()
    
    def max_utility(self):
        return self.connect4_board.max_utility()
    
    def observation_tensor_layout(self):
        return self.connect4_board.observation_tensor_layout()
        
    def observation_tensor_shape(self):
        return self.connect4_board.observation_tensor_shape()
    
    def observation_tensor_size(self):
        return 126
    
    def num_distinct_actions(self):
        return 7
        
if 0:
    game = N_Connect4_Game()
    state = game.new_initial_state()
    while not state.is_terminal():
        action = random.choice(state.legal_actions())
        action_string = state.action_to_string(action)
        print("\nPlayer ", state.current_player(), ", randomly sampled action: ",
                action_string)
        state.apply_action(action)
        print(str(state))
        print('-'*60)
    print('returns ',state.returns())

    
    
    